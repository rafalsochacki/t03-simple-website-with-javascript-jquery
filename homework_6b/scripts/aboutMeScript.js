let baseInfo = {
    "name": "Rafał",
    "surname": "Sochacki",
    "email": "sochacki.rfl@gmail.com",
    "twitter": "noTwitterHere"
};

let project_1 = {"projectLanguage": "Java", "aboutProject": "Main language in the project was Java"};
let project_2 = {"projectLanguage": "JavaScript", "aboutProject": "Main language in the project was JavaScript"};

let cv = {
    "baseInformation": baseInfo,
    "projects": [{"key": "value"}, project_1, project_2]
};


function populateCvData() {
    populateName();
    populateSurname();
    populateTwitter();
    populateEmail();
    populateProjects();
}


function populateName() {
    populate(cv.baseInformation.name, "myName");
}

function populateSurname() {
    populate(cv.baseInformation.surname, "surname");
}

function populateEmail() {
    populate(cv.baseInformation.email, "email")
}

function populateTwitter() {
    populate(cv.baseInformation.twitter, "twitter")
}

function populateProjects() {
    let projectTextTable = [];
    for (let i = 0; i < cv.projects.length; i++) {
        projectTextTable.push("Project Language: " + cv.projects[i].projectLanguage);
        projectTextTable.push("About Project: " + cv.projects[i].aboutProject);
    }

    populate(projectTextTable.join("\n"), "projects");
}

function populate(data, targetId) {
    let targetElement = document.getElementById(targetId);
    targetElement.innerText = data;
}

populateCvData();